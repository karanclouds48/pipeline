variable "tags" {
  description = "Common tags"
  type        = any
}


variable "resource_group_name" {
  description = "Name of the Resource Group"
  type        = string
}


variable "virtual_network_name" {
  description = "Name of the Virtuak Network"
  type        = string
}

variable "subnet_name" {
  description = "Name of the Subnet"
  type        = string
}

variable "ostype" {
  description = "OS Type"
  type        = string
}
variable "os_publisher" {
  description = "Os Publisher name"
  type        = string
}
variable "os_offer" {
  description = "OS Offer type"
  type        = string
}
variable "os_Sku" {
  description = "OS SKU name"
  type        = string
}
variable "os_version" {
  description = "OS Version details"
  type        = string
}
variable "ip_configuration_name" {
  description = "Ip Config Name"
  type        = string
}
variable "private_ip_allocation_type" {
  description = "Ip Allocation Type"
  type        = string
}
variable "location" {
  description = "Location"
  type        = string
}
# variable "private_ip_address" {
#   description = "Private IP Address"
#   type        = string
# }
variable "vm_name" {
  description = "Name of the Virtual Machine"
  type        = string
}
variable "computer_name" {
  description = "Computer name of the VM"
  type        = string
}
variable "vm_size" {
  description = "VM Size"

}
variable "disk_type" {
  description = "Disk Type"

}
variable "storage_disk_create_option" {
  description = "Storage Disk Create Option"

}
variable "storage_disk_caching" {
  description = "Storage disk caching"

}
variable "os_storage_disk_size" {
  description = "OS Storage disk size"

}
variable "enable_accelerated_networking" {
  description = "enable accelerated networking"
}

variable "storage_account_type" {
  description = "Storage account type"

}

variable "domainjoin" {
  description = "domain join"
  default     = null
}

variable "vnet_peering_data" {
  type    = any
  default = {}
}

variable "shared_services_subscription_id" {
  type = string
}

variable "shared_services_spn_client_id" {
  type = string
}

variable "shared_services_spn_client_secret" {
  type      = string
  sensitive = true
}

variable "shared_services_tenant_id" {
  type = string
}

variable "internal_qa_subscription_id" {
  type = string
}
variable "internal_qa_spn_client_id" {
  type = string
}
variable "internal_qa_spn_client_secret" {
  type = string
}
variable "internal_qa_tenant_id" {
  type = string
}