#common tags 
tags = {
  "Owner" : "Daljeet Kaur",
  "Created By" : "daljeet.kaur@invafresh.com",
  "Stakeholders" : "Cloud Team",
  "App" : "Jenkins VM",
  "Tier" : "Infrastructure",
  "Env" : "uat",
  "SLA Severity" : "None",
  "Policy" : "Security",
  "Department" : "Cloud",
  "Product" : "Jenkins VM",
  "Customer" : "Invafresh",
  "Region" : "canadaeast",
}

resource_group_name  = "rg-main-qap-uat-eastus"
virtual_network_name = "vnet-main-qap-uat-eastus"
subnet_name          = "snet-qap-uat-eastus-vmss"
location             = "eastus"

ostype                     = "windows"
os_publisher               = "MicrosoftWindowsServer"
os_offer                   = "WindowsServer"
os_Sku                     = "2019-Datacenter"
os_version                 = "lauat"
ip_configuration_name      = "internal"
private_ip_allocation_type = "Dynamic"
# private_ip_address            = "172.30.17.34"
vm_name                       = "jenkins-uat-vm"
computer_name                 = "jenkins-uat-vm"
vm_size                       = "Standard_E2s_v4"
disk_type                     = "StandardSSD_LRS"
storage_disk_create_option    = "FromImage"
storage_disk_caching          = "ReadWrite"
os_storage_disk_size          = "128"
enable_accelerated_networking = "false"
storage_account_type          = "StandardSSD_LRS"

domainjoin = null

vnet_peering_data = {
  "0" = {
    source_vnet_name              = "vnet-main-qap-uat-eastus"
    source_rg_name                = "rg-main-qap-uat-eastus"
    destination_vnet_name         = "vnet-main-qam-qa-eastus"
    destination_rg_name           = "rg-main-qam-qa-eastus"
    Source_vnet_peering_name      = "mq-main-qam-jenkins"
    Source_vnet_peering_rg        = "rg-main-qap-uat-eastus"
    destination_vnet_peering_name = "qam-main-mq-jenkins"
    destination_vnet_peering_rg   = "rg-main-qam-qa-eastus"
  }
}


