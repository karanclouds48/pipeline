terraform {
  required_providers {
    azurerm = {
      source                = "hashicorp/azurerm"
      configuration_aliases = ["shared", "internal-qa"]
      version               = "3.6.0"
    }
    random = {
      source  = "hashicorp/random"
      version = "3.1.0"
    }

  }
}

# provider "azurerm" {
#   features {

#   }
#   subscription_id = var.shared_services_subscription_id
#   client_id       = var.shared_services_spn_client_id
#   client_secret   = var.shared_services_spn_client_secret
#   tenant_id       = var.shared_services_tenant_id

# }

provider "azurerm" {
  features {

  }
  alias = "shared"

  subscription_id = var.shared_services_subscription_id
  client_id       = var.shared_services_spn_client_id
  client_secret   = var.shared_services_spn_client_secret
  tenant_id       = var.shared_services_tenant_id

}
provider "azurerm" {
  features {

  }
  alias = "internal-qa"

  subscription_id = var.internal_qa_subscription_id
  client_id       = var.internal_qa_spn_client_id
  client_secret   = var.internal_qa_spn_client_secret
  tenant_id       = var.internal_qa_tenant_id

}
