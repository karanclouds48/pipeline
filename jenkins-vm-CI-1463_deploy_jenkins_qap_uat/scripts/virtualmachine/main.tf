data "azurerm_key_vault" "jenkins" {
  name                = "kv-inva-ss-eastus"
  resource_group_name = "rg-shared-inva-eastus"
}
data "azurerm_key_vault_secret" "Jenkins_admin_username" {
  name         = "kv-secret-jenkins-vm-username"
  key_vault_id = data.azurerm_key_vault.jenkins.id
}

data "azurerm_key_vault_secret" "Jenkins_admin_password" {
  name         = "kv-secret-jenkins-vm-password"
  key_vault_id = data.azurerm_key_vault.jenkins.id
}

resource "azurerm_public_ip" "public_ip" {
  name                = "${var.vm_name}-public-ip"
  location            = var.location
  resource_group_name = var.resource_group_name
  allocation_method   = "Static"
}

resource "azurerm_network_interface" "nic" {
  name                          = "${var.vm_name}-nic"
  location                      = var.location
  resource_group_name           = var.resource_group_name
  enable_accelerated_networking = var.enable_accelerated_networking


  ip_configuration {
    name                          = var.ip_configuration_name
    private_ip_address_allocation = var.private_ip_allocation_type
    public_ip_address_id          = azurerm_public_ip.public_ip.id
    subnet_id                     = var.subnet_id
  }

  tags = var.tags
}

resource "azurerm_windows_virtual_machine" "vm" {
  name                = var.vm_name
  resource_group_name = var.resource_group_name
  location            = var.location
  size                = var.vm_size
  admin_username      = data.azurerm_key_vault_secret.Jenkins_admin_username.value
  admin_password      = data.azurerm_key_vault_secret.Jenkins_admin_password.value
  computer_name       = var.computer_name
  network_interface_ids = [
    azurerm_network_interface.nic.id,
  ]

  os_disk {
    name                 = "${var.vm_name}-OS"
    caching              = var.storage_disk_caching
    storage_account_type = var.storage_account_type
  }

  source_image_reference {
    publisher = var.os_publisher
    offer     = var.os_offer
    sku       = var.os_Sku
    version   = var.os_version
  }

  tags = var.tags

}

