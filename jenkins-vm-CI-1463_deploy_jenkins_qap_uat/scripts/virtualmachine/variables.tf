variable "vm_name" {
  description = "Name of virtual machine"
}

variable "location" {
  description = "Location of virtual machine"
}

variable "availability_set_id" {
  default = null
}
variable "zones" {
  description = "(optional)"
  default     = null
}

variable "subnet_id" {
  description = "Name of the Subnet"
}

variable "vm_size" {
  description = "Size of virtual machine"
}

variable "enable_accelerated_networking" {
  description = " Value of network acceleration(true/false)"
}

variable "private_ip_allocation_type" {
  description = "Type of allocation of private ip(Dynamic/static)"
}

variable "ip_configuration_name" {
  description = "Name of ip configuration"
}
variable "storage_account_type" {
  description = "Type of the storage account"
}
variable "tags" {
  description = "Resource Tags"
}
variable "computer_name" {
  description = "Name of the computer"
}

variable "storage_disk_caching" {
  description = "Internal disk caching option of virtual machine"
}
variable "storage_disk_create_option" {
  description = "Inernal disk create option of virtual machine(empty/fromimage etc)"
  default     = "FromImage"
}

variable "resource_group_name" {
  description = "Resource group used in virtual machine"
}


variable "datadisks_type" {
  description = "Type of data disk"
  default     = "Standard_LRS"
}

variable "datadisks_caching" {
  default = "ReadWrite"
}

variable "storage_disk_size" {
  default = "260"
}

variable "ad_ou" {
  description = "ou path"
  default     = null
}

variable "os_publisher" {
  description = "OS publisher Name"
}
variable "os_offer" {
  description = "OS Offer Type"
}
variable "os_Sku" {
  description = "OS SKU Type"
}
variable "os_version" {
  description = "OS Vsersion Name"
} 