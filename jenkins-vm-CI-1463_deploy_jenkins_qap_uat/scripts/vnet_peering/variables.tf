variable "source_vnet_name" {
  description = "Enter the Vnet 1 Name"
}

variable "destination_vnet_name" {
  description = "Enter the VNet 2 Name"
}

variable "source_rg_name" {
  description = "Enter the source Resource Group Name"
}

variable "destination_rg_name" {
  description = "Enter the Destination Resource Group Name"
}

variable "Source_vnet_peering_name" {
  description = "Enter the source Vnet Peering Name"
}

variable "destination_vnet_peering_name" {
  description = "Enter the destination Vnet Peering Name"
}
variable "Source_vnet_peering_rg" {
  description = "Enter the Source Vnet Peering RG"
}
variable "destination_vnet_peering_rg" {
  description = "Enter the Destination Vnet Peering RG"
}