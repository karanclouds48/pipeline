
data "azurerm_virtual_network" "source_vnet" {
  name                = var.source_vnet_name
  resource_group_name = var.source_rg_name
  provider            = azurerm.shared
}

data "azurerm_virtual_network" "destination_vnet" {
  name                = var.destination_vnet_name
  resource_group_name = var.destination_rg_name
  provider            = azurerm.internal-qa
}

resource "azurerm_virtual_network_peering" "source_destination_peering" {
  name                      = var.Source_vnet_peering_name
  resource_group_name       = var.Source_vnet_peering_rg
  virtual_network_name      = data.azurerm_virtual_network.source_vnet.name
  remote_virtual_network_id = data.azurerm_virtual_network.destination_vnet.id
  provider                  = azurerm.shared
}

resource "azurerm_virtual_network_peering" "destination_source__peering" {
  name                      = var.destination_vnet_peering_name
  resource_group_name       = var.destination_vnet_peering_rg
  virtual_network_name      = data.azurerm_virtual_network.destination_vnet.name
  remote_virtual_network_id = data.azurerm_virtual_network.source_vnet.id
  provider                  = azurerm.internal-qa
}