data "azurerm_resource_group" "main_rg" {
  name = var.resource_group_name
}

data "azurerm_virtual_network" "vnet" {
  name                = var.virtual_network_name
  resource_group_name = data.azurerm_resource_group.main_rg.name
}
data "azurerm_subnet" "vmss_subnet" {
  name                 = var.subnet_name
  virtual_network_name = data.azurerm_virtual_network.vnet.name
  resource_group_name  = data.azurerm_resource_group.main_rg.name
}

module "virtual_machine_windows" {
  source                        = "./scripts/virtualmachine"
  resource_group_name           = data.azurerm_resource_group.main_rg.name
  location                      = var.location
  subnet_id                     = data.azurerm_subnet.vmss_subnet.id
  enable_accelerated_networking = var.enable_accelerated_networking
  private_ip_allocation_type    = var.private_ip_allocation_type
  ip_configuration_name         = var.ip_configuration_name
  computer_name                 = var.computer_name
  storage_account_type          = var.storage_account_type
  vm_size                       = var.vm_size
  storage_disk_caching          = var.storage_disk_caching
  storage_disk_create_option    = var.storage_disk_create_option
  storage_disk_size             = var.os_storage_disk_size
  vm_name                       = var.vm_name
  tags                          = var.tags
  os_publisher                  = var.os_publisher
  os_offer                      = var.os_offer
  os_Sku                        = var.os_Sku
  os_version                    = var.os_version
}

module "Vnet_peering" {
  source = "./scripts/vnet_peering"

  providers = {
    azurerm.shared      = azurerm.shared
    azurerm.internal-qa = azurerm.internal-qa
  }
  for_each                      = var.vnet_peering_data
  source_vnet_name              = each.value.source_vnet_name
  source_rg_name                = each.value.source_rg_name
  destination_vnet_name         = each.value.destination_vnet_name
  destination_rg_name           = each.value.destination_rg_name
  Source_vnet_peering_name      = each.value.Source_vnet_peering_name
  Source_vnet_peering_rg        = each.value.Source_vnet_peering_rg
  destination_vnet_peering_name = each.value.destination_vnet_peering_name
  destination_vnet_peering_rg   = each.value.destination_vnet_peering_rg

}
